let i = 0;
const modeList = [
  "normal",
  // 'multiply',
  // 'screen',
  // 'overlay',
  // 'darken',
  // 'lighten',
  // 'color-dodge',
  // 'color-burn',
  // 'hard-light',
  // 'soft-light',
  "difference"
  // 'exclusion',
  // 'hue',
  // 'saturation',
  // 'color',
  // 'luminosity'
];
const elem = document.getElementsByClassName("apply-effect-here")[0];
const effectRow = document.getElementsByClassName("effect-row")[0];

modeList.forEach(item => {
  const effectButton = document.createElement("input");
  effectButton.setAttribute("type", "button");
  effectButton.setAttribute("value", item);
  effectButton.classList.add("effectButton");
  effectButton.addEventListener(
    "click",
    () => (elem.style.mixBlendMode = item)
  );
  effectRow.appendChild(effectButton);
});
